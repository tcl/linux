/**
 * Driver to expose the SL50 hardware revision
 *
 * Copyright (C) 2017 Collabora Ltd.
 * Author: Enric Balletbo Serra <enric.balletbo@collabora.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/gpio/consumer.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/sysfs.h>

struct tcl_hardware_drv_data {
	struct gpio_descs *gpiod_major;
	struct gpio_descs *gpiod_minor;

	struct platform_device *pdev;

	u8 major;
	u8 minor;
};

static ssize_t tcl_hardware_revision_show(struct device *dev,
					  struct device_attribute *attr,
					  char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct tcl_hardware_drv_data *data = platform_get_drvdata(pdev);
	u16 revision;

	revision = data->minor | (data->major << 8);

	return scnprintf(buf, PAGE_SIZE, "%d\n", revision);
}

static ssize_t tcl_hardware_pcb_rev_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct tcl_hardware_drv_data *data = platform_get_drvdata(pdev);

	return scnprintf(buf, PAGE_SIZE, "%d\n", data->major);
}

static ssize_t tcl_hardware_bom_rev_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct tcl_hardware_drv_data *data = platform_get_drvdata(pdev);

	return scnprintf(buf, PAGE_SIZE, "%d\n", data->minor);
}

static DEVICE_ATTR(revision, 0444, tcl_hardware_revision_show, NULL);
static DEVICE_ATTR(pcb_rev, 0444, tcl_hardware_pcb_rev_show, NULL);
static DEVICE_ATTR(bom_rev, 0444, tcl_hardware_bom_rev_show, NULL);

static struct attribute *tcl_hardware_attrs[] = {
	&dev_attr_revision.attr,
	&dev_attr_pcb_rev.attr,
	&dev_attr_bom_rev.attr,
	NULL,
};

static const struct attribute_group tcl_hardware_attr_group = {
	.attrs = tcl_hardware_attrs,
};

static int tcl_hardware_probe(struct platform_device *pdev)
{
	struct tcl_hardware_drv_data *data;
	struct device *dev = &pdev->dev;
	int ret;
	int val;
	int i;

	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	data->gpiod_major = devm_gpiod_get_array(dev, "revision-major",
						 GPIOD_IN);
	if (IS_ERR(data->gpiod_major)) {
		dev_err(dev, "unable to claim revision-major gpios\n");
		return PTR_ERR(data->gpiod_major);
	}

	data->gpiod_minor = devm_gpiod_get_array(dev, "revision-minor",
						 GPIOD_IN);
	if (IS_ERR(data->gpiod_minor)) {
		dev_err(dev, "unable to claim revision-minor gpios\n");
		return PTR_ERR(data->gpiod_minor);
	}

	if (data->gpiod_minor->ndescs > 8) {
		dev_err(dev, "too much revision-minor gpios (max. 8)\n");
		return -EINVAL;
	}

	for (i = 0; i < data->gpiod_minor->ndescs; i++) {
                val = gpiod_get_value_cansleep(data->gpiod_minor->desc[i]);
                if (val < 0) {
                        dev_err(dev, "error reading gpio %d: %d\n",
                                desc_to_gpio(data->gpiod_minor->desc[i]), val);
                        return val;
                }

		data->minor |= (val << i);
		/*
		 * Export the gpios to not break compatibility with some
		 * userspace tools
		 */
		gpiod_export(data->gpiod_minor->desc[i], false);
        }

	if (data->gpiod_major->ndescs > 8) {
		dev_err(dev, "too much revision-major gpios (max. 8)\n");
		return -EINVAL;
	}

	for (i = 0; i < data->gpiod_major->ndescs; i++) {
                val = gpiod_get_value_cansleep(data->gpiod_major->desc[i]);
                if (val < 0) {
                        dev_err(dev, "error reading gpio %d: %d\n",
                                desc_to_gpio(data->gpiod_major->desc[i]), val);
                        return val;
                }

		data->major |= (val << i);
		/*
		 * Export the gpios to not break compatibility with some
		 * userspace tools
		 */
		gpiod_export(data->gpiod_major->desc[i], false);
        }

	platform_set_drvdata(pdev, data);
        data->pdev = pdev;

	ret = sysfs_create_group(&dev->kobj, &tcl_hardware_attr_group);
	if (ret) {
		dev_err(dev, "failed to create sysfs files\n");
		return ret;
	}

	return 0;
}

static int tcl_hardware_remove(struct platform_device *pdev)
{
	sysfs_remove_group(&pdev->dev.kobj, &tcl_hardware_attr_group);

	return 0;
}

static struct of_device_id tcl_hardware_of_match[] = {
	{ .compatible = "tcl,tcl-hardware", },
	{ /* sentinel*/ },
};
MODULE_DEVICE_TABLE(of, tcl_hardware_of_match);

static struct platform_driver tcl_hardware_platform_driver = {
	.probe		= tcl_hardware_probe,
	.remove		= tcl_hardware_remove,
	.driver		= {
		.name		= "tcl-hardware",
		.of_match_table	= tcl_hardware_of_match,
	},
};

module_platform_driver(tcl_hardware_platform_driver);

MODULE_DESCRIPTION("Driver to expose the SL50 hardware revision");
MODULE_AUTHOR("Enric Balletbo Serra <enric.balletbo@collabora.com>");
MODULE_LICENSE("GPL v2");
