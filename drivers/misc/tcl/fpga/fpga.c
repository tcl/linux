/**
 * drivers/misc/tcl/fpga - Driver to handle the gpios connected to the FPGA
 *
 * Copyright (C) 2017 Collabora Ltd.
 * Author: Enric Balletbo i Serra <enric.balletbo@collabora.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/gpio/consumer.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/of_platform.h>
#include <linux/module.h>

struct fpga_platform_data {
	struct gpio_desc *fpga_nrst;
	struct gpio_desc *fpga_en;
	struct gpio_desc *fpga_program;
};

static int fpga_probe(struct platform_device *pdev)
{
	struct fpga_platform_data *pdata;

	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata)
		return -ENOMEM;

	pdata->fpga_nrst = devm_gpiod_get(&pdev->dev, "reset", GPIOD_OUT_LOW);
	if (IS_ERR(pdata->fpga_nrst)) {
		dev_err(&pdev->dev, "unable to claim RESET gpio\n");
		return PTR_ERR(pdata->fpga_nrst);
	}

	pdata->fpga_en = devm_gpiod_get(&pdev->dev, "enable", GPIOD_OUT_HIGH);
	if (IS_ERR(pdata->fpga_en)) {
		dev_err(&pdev->dev, "unable to claim ENABLE gpio\n");
		return PTR_ERR(pdata->fpga_en);
	}

	pdata->fpga_program = devm_gpiod_get(&pdev->dev, "program",
					     GPIOD_OUT_LOW);
	if (IS_ERR(pdata->fpga_program)) {
		dev_err(&pdev->dev, "unable to claim PROGRAM gpio\n");
		return PTR_ERR(pdata->fpga_program);
	}

	msleep(100);

	gpiod_set_value_cansleep(pdata->fpga_nrst, 0);

	gpiod_export(pdata->fpga_nrst, false);
	gpiod_export(pdata->fpga_en, false);
	gpiod_export(pdata->fpga_program, false);

	return 0;
}

static int fpga_remove(struct platform_device *pdev)
{
	return 0;
}

static struct of_device_id fpga_of_match[] = {
	{ .compatible = "tcl,tcl-fpga", },
	{},
};
MODULE_DEVICE_TABLE(of, fpga_of_match);

static struct platform_driver fpga_platform_driver = {
	.probe		= fpga_probe,
	.remove		= fpga_remove,
	.driver		= {
		.name		= "tcl-fpga",
		.of_match_table	= fpga_of_match,
	},
};

module_platform_driver(fpga_platform_driver);

MODULE_DESCRIPTION("Driver to handle the gpios connected to the FPGA");
MODULE_AUTHOR("Enric Balletbo i Serra <enric.balletbo@collabora.com>");
MODULE_LICENSE("GPL v2");
