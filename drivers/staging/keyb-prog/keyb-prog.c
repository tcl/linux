#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/io.h>

#define GPIO_BASE 0x44E10000

void __iomem *io;

int init_module(void)
{
	printk(KERN_INFO "Changing SPI0 pins to GPIOs\n");

	io = ioremap(GPIO_BASE, SZ_4K);

	writel(0x37, io + 0x950);
	writel(0x37, io + 0x954);
	writel(0x37, io + 0x958);

	/* 
	 * A non 0 return means init_module failed; module can't be loaded. 
	 */
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Configuring again SPI0 pins\n");

	writel(0x30, io + 0x950);
	writel(0x30, io + 0x954);
	writel(0x30, io + 0x958);

	iounmap(io);
}
