/*
 * TI ADC MUX MFD driver
 *
 * Copyright (C) 2016 Toby Chruchill Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/init.h>
//#include <linux/slab.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/of_device.h>

#include <linux/mfd/core.h>
#include <linux/mfd/tps65217.h>

enum tps65217_muxctrl_sel {
	TIADC_MUXCTRL_SEL_DISABLED = 0,
	TIADC_MUXCTRL_SEL_VBAT,
	TIADC_MUXCTRL_SEL_VSYS,
	TIADC_MUXCTRL_SEL_VTS,
	TIADC_MUXCTRL_SEL_VICHARGE,
	TIADC_MUXCTRL_SEL_MUX_IN,
};

static const char * const tp65217_voltage1_sel[] = { "disabled", "vbat",
					"vsys", "vts", "vicharge", "mux_in" };

struct tps65217_mux {
	struct tps65217 *tps;
	enum tps65217_muxctrl_sel muxctrl;
};

static ssize_t tps65217_mux_show_voltage1_muxctrl(struct device *dev,
						  struct device_attribute *attr,
						  char *buf)
{
	struct tps65217_mux *mux = dev_get_drvdata(dev);

	return scnprintf(buf, PAGE_SIZE, "%s\n", tp65217_voltage1_sel[mux->muxctrl]);
}

static ssize_t tps65217_mux_store_voltage1_muxctrl(struct device *dev,
						   struct device_attribute *attr,
						   const char *buf, size_t count)
{
	struct tps65217_mux *mux = dev_get_drvdata(dev);
	int sel, ret;

	for (sel = 0; sel < ARRAY_SIZE(tp65217_voltage1_sel); sel++) {
		if (!strncmp(buf, tp65217_voltage1_sel[sel],
			     strlen(tp65217_voltage1_sel[sel]))) {
			ret = tps65217_set_bits(mux->tps, TPS65217_REG_MUXCTRL,
						TPS65217_MUXCTRL_MUX_MASK, sel,
						TPS65217_PROTECT_NONE);
			if (ret)
				return ret;

			mux->muxctrl = sel;

			return count;
		}
	}

	return -EINVAL;
}

static DEVICE_ATTR(in_voltage1_muxctrl, S_IRUGO | S_IWUSR,
		   tps65217_mux_show_voltage1_muxctrl,
		   tps65217_mux_store_voltage1_muxctrl);

static int tps65217_mux_probe(struct platform_device *pdev)
{
	struct tps65217 *tps = dev_get_drvdata(pdev->dev.parent);
	struct device_node *np = pdev->dev.of_node;
	struct tps65217_mux *mux;
	int ret;

	if (!np) {
		dev_err(&pdev->dev, "No charger OF node\n");
		return -ENODEV;
	}

	mux = devm_kzalloc(&pdev->dev, sizeof(*mux), GFP_KERNEL);
	if (mux == NULL)
		return -ENOMEM;

	mux->tps = tps;

	platform_set_drvdata(pdev, mux);

	ret = device_create_file(&pdev->dev, &dev_attr_in_voltage1_muxctrl);
	if (ret) {
		dev_err(&pdev->dev, "failed: create sysfs entry\n");
		return ret;
	}

	return 0;
}

static int tps65217_mux_remove(struct platform_device *pdev)
{
	device_remove_file(&pdev->dev, &dev_attr_in_voltage1_muxctrl);

	return 0;
}

static const struct of_device_id tps65217_mux_of_match[] = {
	{ .compatible = "ti,tps65217-mux", },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, tps65217_mux_of_match);

static struct platform_driver tps65217_mux_driver = {
	.probe	= tps65217_mux_probe,
	.remove = tps65217_mux_remove,
	.driver	= {
		.name	= "tps65217-mux",
		.of_match_table = tps65217_mux_of_match,
	},

};
module_platform_driver(tps65217_mux_driver);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Enric Balletbo Serra <enric.balletbo@collabora.com>");
MODULE_DESCRIPTION("TPS65217 ADC multiplexer");
