/*
 * Battery monitor driver for Toby Churchill SBS Batteries
 *
 * Copyright (c) 2017, Collabora Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>

#define SBS_MEMORY_MAP_SIZE		128
/* Charging voltage, 2 bytes */
#define SBS_CHARGING_VOLTAGE		0x0a
/* Design voltage, 2 bytes */
#define SBS_DESIGN_VOLTAGE		0x0c
/* Fast charging current, 2 bytes */
#define SBS_FAST_CHARGING_CURRENT	0x0e
/* Max T, Low T, 2 bytes */
#define SBS_MAX_LOW_TEMPERATURE		0x10
/* Pack capacity, 2 bytes */
#define SBS_PACK_CAPACITY		0x12
/* Serial number, 2 bytes */
#define SBS_SERIAL_NUMBER		0x18
/* Manufacturer name, 16 bytes */
#define SBS_MANUFACTURER_NAME		0x20
/* Model name, 16 bytes */
#define SBS_MODEL_NAME			0x30
/* Device chemistry, 5 bytes */
#define SBS_DEVICE_CHEMISTRY		0x40
/* Voltage now, 2 bytes */
#define SBS_VOLTAGE_NOW			0x70
/* Current now, 2 bytes */
#define SBS_CURRENT_NOW			0x72
/* Battery Status, 2 bytes */
#define SBS_BATTERY_STATUS		0x74
# define BATTERY_STATUS_CHARGING	0
# define BATTERY_STATUS_NOT_PRESENT	BIT(7)
# define BATTERY_STATUS_DISCHARGING	BIT(6)
# define BATTERY_STATUS_FULLY_CHARGED	BIT(5)
/* State of charge in percentage, 1 byte */
#define SBS_STATE_OF_CHARGE		0x76
/* Cycle count, 2 bytes */
#define SBS_CYCLE_COUNT			0x77
/* Remaining capacity, 2 bytes */
#define SBS_REM_CAPACITY		0x79
/* Actual capcacity, 2 bytes */
#define SBS_ACTUAL_CAPACITY		0x7b
/* Firmware version, 1 byte */
#define SBS_FIRMWARE_VERSION		0x7d

/* Command to reset battery after replacement */
#define SBS_CMD_BATTERY_RESET		(SBS_MEMORY_MAP_SIZE + 1)

/* MM SIZE + START(u16) + CHECKSUM(u16) */
#define SPI_MSG_LENGTH		(SBS_MEMORY_MAP_SIZE + 4)
#define SPI_MSG_DATA_BP		2
/* MSB checksum byte position */
#define SPI_MSG_CSUM_BP		(2 + SBS_MEMORY_MAP_SIZE)
#define SPI_MSG_START_TOKEN	0xb00b

struct tcl_sbs_battery_data {
	struct spi_device *spi;
	struct power_supply *bat;

	struct mutex		work_lock; /* protect work data */
	struct delayed_work	bat_work;

	u8 map[SBS_MEMORY_MAP_SIZE];

	char model_name[16];
	char manufacturer_name[16];
	char serial_number[5];

	int technology;
	int voltage_uvolt;		/* units of uV */
	int current_uamp;		/* units of uA */
	int rated_capacity;		/* units of µAh */
	int full_capacity;		/* units of µAh */
	int rem_capacity;		/* units of µAh */
	int cycle_count;
	int charge_state;		/* percentage */
	int life_sec;			/* units of seconds */
	int status;			/* state of charge */
	int design_voltage;		/* units of uV */

	u8 fw_version;			/* firmware version */
	bool presence;			/* battery presence */
	bool program;			/* program mode */
};

#define MAX_KEYLENGTH 256
struct battery_property_map {
	int value;
	char const *key;
};

static struct battery_property_map map_technology[] = {
	{ POWER_SUPPLY_TECHNOLOGY_NiMH, "NiMH" },
	{ POWER_SUPPLY_TECHNOLOGY_LION, "LION" },
	{ POWER_SUPPLY_TECHNOLOGY_LIPO, "LIPO" },
	{ POWER_SUPPLY_TECHNOLOGY_LiFe, "LiFe" },
	{ POWER_SUPPLY_TECHNOLOGY_NiCd, "NiCd" },
	{ POWER_SUPPLY_TECHNOLOGY_LiMn, "LiMn" },
	{ -1,				NULL   },
};

static int map_get_value(struct battery_property_map *map, const char *key,
			 int def_val)
{
	char buf[MAX_KEYLENGTH];
	int cr;

	strncpy(buf, key, MAX_KEYLENGTH);
	buf[MAX_KEYLENGTH - 1] = '\0';

	cr = strnlen(buf, MAX_KEYLENGTH) - 1;
	if (buf[cr] == '\n')
		buf[cr] = '\0';

	while (map->key) {
		if (strncasecmp(map->key, buf, MAX_KEYLENGTH) == 0)
			return map->value;
		map++;
	}

	return def_val;
}

static int tcl_sbs_battery_read_status(struct tcl_sbs_battery_data *data)
{
	int i, err;
	int csum = 0;
	u8 buf[SBS_MEMORY_MAP_SIZE], technology[5];
	unsigned int uval;
	int sval;
	struct spi_device *spi = data->spi;

	for (i = 0; i < SBS_MEMORY_MAP_SIZE; i++) {
		err = spi_write(spi, &i, 1);
		if (err) {
			dev_err(&spi->dev, "failed to write command\n");
			return err;
		}

		err = spi_read(spi, &buf[i], 1);
		if (err) {
			dev_err(&spi->dev, "failed to write command\n");
			return err;
		}
	}

	/* Calculate the data checksum */
	for (i = 0; i < SBS_MEMORY_MAP_SIZE - 2; i++)
		csum += buf[i];

	/* Verify the checksum */
	uval = (buf[SBS_MEMORY_MAP_SIZE - 2] << 8) |
		buf[SBS_MEMORY_MAP_SIZE - 1];
	if (csum != uval) {
		dev_dbg(&spi->dev,
			"message received with invalid checksum (%d != %d)\n",
			csum, uval);
		print_hex_dump_debug("tcl-sbs-battery: ", DUMP_PREFIX_OFFSET,
				     16, 1, buf, SBS_MEMORY_MAP_SIZE, false);
		return -ENOMSG;
	}

	/* Update memory map with the new data */
	memcpy(data->map, buf, SBS_MEMORY_MAP_SIZE);

	strncpy(data->model_name, &data->map[SBS_MODEL_NAME], 16);

	strncpy(data->manufacturer_name, &data->map[SBS_MANUFACTURER_NAME],
		16);

	strncpy(technology, &data->map[SBS_DEVICE_CHEMISTRY], 5);

	uval = (u16)((data->map[SBS_SERIAL_NUMBER + 1] << 8) |
		data->map[SBS_SERIAL_NUMBER]);
	snprintf(data->serial_number, ARRAY_SIZE(data->serial_number), "%04d",
		 uval);

	data->technology = map_get_value(map_technology, technology,
					 POWER_SUPPLY_TECHNOLOGY_UNKNOWN);

	data->voltage_uvolt = (u16)((data->map[SBS_VOLTAGE_NOW + 1] << 8) |
			    data->map[SBS_VOLTAGE_NOW]);
	data->voltage_uvolt *= 1000;	/* convert from mV to uV */

	sval = (s16)((data->map[SBS_CURRENT_NOW + 1] << 8) |
		data->map[SBS_CURRENT_NOW]);
	data->current_uamp = sval;
	data->current_uamp *= 1000;	/* convert from mA to uA */

	data->rated_capacity = (u16)((data->map[SBS_PACK_CAPACITY + 1] << 8) |
				data->map[SBS_PACK_CAPACITY]);
	data->rated_capacity *= 1000;	/* convert from mAh to uAh */

	data->full_capacity = (u16)((data->map[SBS_ACTUAL_CAPACITY + 1] << 8) |
				data->map[SBS_ACTUAL_CAPACITY]);
	data->full_capacity *= 1000;	/* convert from mAh to uAh */

	data->rem_capacity = (u16)((data->map[SBS_REM_CAPACITY + 1] << 8) |
				data->map[SBS_REM_CAPACITY]);
	data->rem_capacity *= 1000;	/* convert from mAh to uAh */

	data->presence = true;

	uval = (u16)((data->map[SBS_BATTERY_STATUS + 1] << 8) |
		data->map[SBS_BATTERY_STATUS]);
	if (uval == BATTERY_STATUS_CHARGING)
		data->status = POWER_SUPPLY_STATUS_CHARGING;
	else if (uval == BATTERY_STATUS_DISCHARGING)
		data->status = POWER_SUPPLY_STATUS_DISCHARGING;
	else if (uval == BATTERY_STATUS_FULLY_CHARGED)
		data->status = POWER_SUPPLY_STATUS_FULL;
	else if (uval == BATTERY_STATUS_NOT_PRESENT)
		data->presence = false;
	else
		data->status = POWER_SUPPLY_STATUS_UNKNOWN;

	data->cycle_count = (u16)((data->map[SBS_CYCLE_COUNT + 1] << 8) |
			     data->map[SBS_CYCLE_COUNT]);

	data->charge_state = data->map[SBS_STATE_OF_CHARGE];

	uval = (data->charge_state * (data->full_capacity / 1000)) / 100;
	if (data->current_uamp)
		data->life_sec = (3600l * uval) / (data->current_uamp / 1000);

	data->design_voltage = (u16)((data->map[SBS_DESIGN_VOLTAGE + 1] << 8) |
			    data->map[SBS_DESIGN_VOLTAGE]);
	data->design_voltage *= 1000;	/* convert from mV to uV */

	data->fw_version = data->map[SBS_FIRMWARE_VERSION];

	return 0;
}

static void tcl_sbs_battery_work(struct work_struct *work)
{
	struct tcl_sbs_battery_data *data = container_of(work,
		struct tcl_sbs_battery_data, bat_work.work);

	/* Update values */
	mutex_lock(&data->work_lock);
	tcl_sbs_battery_read_status(data);
	mutex_unlock(&data->work_lock);

	schedule_delayed_work(&data->bat_work, HZ * 60);
}

static char *tcl_sbs_battery_supplied_to[] = {
	"tps65217-charger",
};


static int tcl_sbs_battery_get_property(struct power_supply *psy,
					  enum power_supply_property psp,
					  union power_supply_propval *val)
{
	struct tcl_sbs_battery_data *data = power_supply_get_drvdata(psy);

	/*
	 * If battery is not present this driver just makes no sense as
	 * can report wrong values, so handle that situation in the way
	 * that if battery is not present the only property that makes
	 * sense is POWER_SUPPLY_PROP_PRESENT, other properties should
	 * only work, and makes sense if battery is detected.
	 */
	if (psp == POWER_SUPPLY_PROP_PRESENT) {
		val->intval = data->presence ? 1 : 0;
		return 0;
	}

	if (!data->presence) {
		return -ENODEV;
	}

	/*
	 * If fw_version is 0 that means the firmware is not flashed, in such
	 * case properties are invalid.
	 */
	if (!data->fw_version)
		return -ENODEV;

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = data->status;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = data->voltage_uvolt;
		break;
	case POWER_SUPPLY_PROP_CURRENT_NOW:
		val->intval = data->current_uamp;
		break;
	case POWER_SUPPLY_PROP_CHARGE_FULL_DESIGN:
		val->intval = data->rated_capacity;
		break;
	case POWER_SUPPLY_PROP_CHARGE_FULL:
		val->intval = data->full_capacity;
		break;
	case POWER_SUPPLY_PROP_CHARGE_NOW:
		val->intval = data->rem_capacity;
		break;
	case POWER_SUPPLY_PROP_TIME_TO_EMPTY_NOW:
		val->intval = data->life_sec;
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = data->charge_state;
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = data->technology;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_MAX_DESIGN:
		val->intval = data->design_voltage;
		break;
	case POWER_SUPPLY_PROP_CYCLE_COUNT:
		val->intval = data->cycle_count;
		break;
	case POWER_SUPPLY_PROP_MODEL_NAME:
		val->strval = data->model_name;
		break;
	case POWER_SUPPLY_PROP_MANUFACTURER:
		val->strval = data->manufacturer_name;
		break;
	case POWER_SUPPLY_PROP_SERIAL_NUMBER:
		val->strval = data->serial_number;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static enum power_supply_property tcl_sbs_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CHARGE_FULL_DESIGN,
	POWER_SUPPLY_PROP_CHARGE_FULL,
	POWER_SUPPLY_PROP_CHARGE_NOW,
	POWER_SUPPLY_PROP_TIME_TO_EMPTY_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_TECHNOLOGY,
	POWER_SUPPLY_PROP_VOLTAGE_MAX_DESIGN,
	POWER_SUPPLY_PROP_CYCLE_COUNT,
	/* Properties of type `const char *' */
	POWER_SUPPLY_PROP_MODEL_NAME,
	POWER_SUPPLY_PROP_MANUFACTURER,
	POWER_SUPPLY_PROP_SERIAL_NUMBER,
	/* Properties from charger */
	POWER_SUPPLY_PROP_PRESENT,
};

static const struct power_supply_desc tcl_sbs_battery_desc = {
	.name			= "battery-monitor",
	.type			= POWER_SUPPLY_TYPE_BATTERY,
	.properties		= tcl_sbs_battery_props,
	.num_properties		= ARRAY_SIZE(tcl_sbs_battery_props),
	.get_property		= tcl_sbs_battery_get_property,
};

static ssize_t tcl_sbs_battery_fw_version_show(struct device *dev,
					       struct device_attribute *attr,
					       char *buf)
{
	struct tcl_sbs_battery_data *data = dev_get_drvdata(dev->parent);

	return scnprintf(buf, PAGE_SIZE, "%d\n", data->fw_version);
}

static DEVICE_ATTR(fw_version, 0444, tcl_sbs_battery_fw_version_show, NULL);

static ssize_t tcl_sbs_battery_program_show(struct device *dev,
					    struct device_attribute *attr,
					    char *buf)
{
	struct tcl_sbs_battery_data *data = dev_get_drvdata(dev->parent);

	return scnprintf(buf, PAGE_SIZE, "%d\n", (int)data->program);
}

/*
 * The PDI_CLK/RESET is shared and was configured as gpmc_ad12.pru0_pru_r30_14
 * by default, but then acts as output and is low by default triggering the
 * reset of the MCU. One of the problem with this is that on a power cycle or
 * when you resume PDI_CLK/RESET goes down. When this pin is muxed to the PRU
 * you can't control it, only from the PRU firmware you can do this, so every
 * time this pin is muxed to be used for the PRU goes down and resets the MCU.
 *
 * To deal with this introduce a new sysfs entry that enables/disables the
 * programming, setting by default the pin as input with a pullup and muxing
 * to PRU only when you want to program the MCU.
 *
 * TODO: That's an ugly hack as the pin is hardcoded in this code but there is
 * no common interface to do this, I suspect that something like the pm calls
 * 'pinctrl_pm_select_state' should be implemented to do the things well. Note
 * that the control module registers cannot be accesed from userland neither
 * the PRU (needs privileged mode) so pinmux needs to be set from a kernel
 * module.
 */
#define GPIO_BASE 0x44E10000

void __iomem *io;

static ssize_t tcl_sbs_battery_program_store(struct device *dev,
					     struct device_attribute *attr,
					     const char *buf,
					     size_t count)
{
	long program;
	struct tcl_sbs_battery_data *data = dev_get_drvdata(dev->parent);

	if (kstrtol(buf, 10, &program) < 0)
		return -EINVAL;

	if (program) {
		dev_info(&data->bat->dev, "entering in programming modei\n");
		/* stop polling the MCU */
		cancel_delayed_work_sync(&data->bat_work);
		/* connect pdi_clk/reset pin to the pru */
		writel(0x36, io + 0x830); /* gpmc_ad12.pru0_pru_r30_14 */
		data->program = true;
	} else {
		dev_info(&data->bat->dev, "entering in running mode\n");
		/* reconfigure pdi_clk/reset pin as input */
		writel(0x37, io + 0x830); /* gpmc_ad12.gpio1_12 */
		/* re-start polling the MCU */
		schedule_delayed_work(&data->bat_work, 0);
		data->program = false;
	}

	return count;
}

static DEVICE_ATTR(program, 0644, tcl_sbs_battery_program_show,
		   tcl_sbs_battery_program_store);

static ssize_t battery_reset_store(struct device *dev,
				   struct device_attribute *attr,
				   const char *buf,
				   size_t count)
{
	struct tcl_sbs_battery_data *data = dev_get_drvdata(dev->parent);
	struct spi_device *spi = data->spi;
	u8 cmd, res;
	bool reset;
	int err;

	err = kstrtobool(buf, &reset);
	if (err)
		return err;
	if (!reset)
		return count;

	/* stop polling the MCU */
	cancel_delayed_work_sync(&data->bat_work);

	cmd = SBS_CMD_BATTERY_RESET;
	err = spi_write(spi, &cmd, 1);
	if (err) {
		dev_err(&spi->dev, "failed to write command\n");
		count = err;
		goto restart_delayed_work;
	}

	err = spi_read(spi, &res, 1);
	if (err) {
		dev_err(&spi->dev, "failed to read command result\n");
		count = err;
		goto restart_delayed_work;
	}

	if (res) {
		dev_err(&spi->dev, "invalid command %u\n", cmd);
		count = -EINVAL;
	}

restart_delayed_work:
	/* re-start polling the MCU */
	schedule_delayed_work(&data->bat_work, 0);

	return count;
}

static DEVICE_ATTR_WO(battery_reset);

static struct attribute *tcl_sbs_battery_attrs[] = {
	&dev_attr_battery_reset.attr,
	&dev_attr_fw_version.attr,
	&dev_attr_program.attr,
	NULL,
};

static const struct attribute_group tcl_sbs_battery_attr_group = {
	.attrs = tcl_sbs_battery_attrs,
};

static int tcl_sbs_battery_probe(struct spi_device *spi)
{
	struct tcl_sbs_battery_data *data;
	struct power_supply_config psy_cfg = {};
	int ret;

	data = devm_kzalloc(&spi->dev, sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	data->spi = spi;
	psy_cfg.of_node = spi->dev.of_node;
	psy_cfg.supplied_to = tcl_sbs_battery_supplied_to;
	psy_cfg.num_supplicants = ARRAY_SIZE(tcl_sbs_battery_supplied_to);
	psy_cfg.drv_data = data;

	mutex_init(&data->work_lock);

	INIT_DELAYED_WORK(&data->bat_work, tcl_sbs_battery_work);

	spi_set_drvdata(spi, data);

	io = ioremap(GPIO_BASE, SZ_4K);
	/* Get initial status */
	tcl_sbs_battery_read_status(data);

	data->bat = devm_power_supply_register(&spi->dev,
					       &tcl_sbs_battery_desc,
					       &psy_cfg);
	if (IS_ERR(data->bat))
		return PTR_ERR(data->bat);

	ret = sysfs_create_group(&data->bat->dev.kobj,
				 &tcl_sbs_battery_attr_group);
	if (ret) {
		dev_err(&data->bat->dev, "failed to create sysfs files\n");
		return ret;
	}

	schedule_delayed_work(&data->bat_work, 0);

	return 0;
}

static int tcl_sbs_battery_remove(struct spi_device *spi)
{
	struct tcl_sbs_battery_data *data = spi_get_drvdata(spi);

	sysfs_remove_group(&data->bat->dev.kobj, &tcl_sbs_battery_attr_group);

	cancel_delayed_work_sync(&data->bat_work);

	return 0;
}

#ifdef CONFIG_OF
static const struct of_device_id tcl_sbs_battery_of_match[] = {
	{ .compatible = "tcl,tcl-sbs-battery", },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, tcl_sbs_battery_of_match);
#endif

static struct spi_driver tcl_sbs_battery_driver = {
	.driver = {
		.name		= "tcl-sbs-battery",
		.owner		= THIS_MODULE,
		.of_match_table = of_match_ptr(tcl_sbs_battery_of_match),
	},
	.probe	= tcl_sbs_battery_probe,
	.remove	= tcl_sbs_battery_remove,
};
module_spi_driver(tcl_sbs_battery_driver);

MODULE_ALIAS("spi:tcl-sbs-battery");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Enric Balletbo Serra <enric.balletbo@collabora.com>");
MODULE_DESCRIPTION("Toby Churchill battery monitor driver");
