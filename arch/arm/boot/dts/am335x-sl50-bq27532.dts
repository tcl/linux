/*
 * Copyright (C) 2015 Toby Churchill - http://www.toby-churchill.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;

#include "am335x-sl50-common.dtsi"

/ {
	model = "Toby Churchill SL50 Series with BQ27532 DM";
	compatible = "tcl,am335x-sl50-bq27532", "ti,am33xx";

	bat: battery {
		compatible = "simple-battery";
		voltage-min-design-microvolt = <3500000>;
		energy-full-design-microwatt-hours = <25440000>;
		charge-full-design-microamp-hours = <7000000>;
	};
};

&i2c0 {
	clock-frequency = <200000>;

	bq27532: fuel-guage@55 {
		compatible = "ti,bq27532";
		pinctrl-names = "default";
		pinctrl-0 = <&fuel_guage_irq_pins>;
		reg = <0x55>;
		monitored-battery = <&bat>;
		interrupt-parent = <&gpio0>;
		interrupts = <27 IRQ_TYPE_EDGE_FALLING>; /* GPIO0_27 */
	};
};

&audio_codec {
	gpio-reset = <&gpio0 19 GPIO_ACTIVE_HIGH>;
};

&am33xx_pinmux {
	fuel_guage_irq_pins: pinmux_fuel_guage_irq_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x82c, PIN_INPUT_PULLUP | MUX_MODE7)     /* gpmc_ad11.gpio0_27 */
		>;
	};
};
