Binding for the Toby Churchill SBS battery monitor

The node for this device must be a child of a SPI controller node, as the
device communicates via SPI.

Required properties :
 - compatible : "tcl,tcl-sbs-battery"

See spi-bus.txt for additional properties not specific to this device.

Example:

	battery: smart-battery@0 {
		compatible = "tcl,tcl-sbs-battery";
		reg = <0>;
		spi-max-frequency = <100000>;
	};
