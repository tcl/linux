Common MDIO bus properties.

These are generic properties that can apply to any MDIO bus.

Optional properties:
- reset-gpios: List of one or more GPIOs that control the RESET lines
  of the PHYs on that MDIO bus.
- reset-delay-us: RESET pulse width in microseconds as per PHY datasheet.

A list of child nodes, one per device on the bus is expected. These
should follow the generic phy.txt, or a device specific binding document.

Example :
This example shows these optional properties, plus other properties
required for the TI Davinci MDIO driver.

	davinci_mdio: ethernet@0x5c030000 {
		compatible = "ti,davinci_mdio";
		reg = <0x5c030000 0x1000>;
		#address-cells = <1>;
		#size-cells = <0>;

		reset-gpios = <&gpio2 5 GPIO_ACTIVE_LOW>;
		reset-delay-us = <2>;   /* PHY datasheet states 1us min */

		ethphy0: ethernet-phy@1 {
			reg = <1>;
		};

		ethphy1: ethernet-phy@3 {
			reg = <3>;
		};
	};
